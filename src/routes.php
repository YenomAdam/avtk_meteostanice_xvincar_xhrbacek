<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    try {

        $stmt = $this->db->prepare("
        SELECT stav_dne FROM mereni
        FULL JOIN cidlo_svetla
        USING(id_mereni)
        WHERE stav_dne = 'vychod' or stav_dne = 'zapad'
        ORDER BY(mereni) DESC
        FETCH FIRST 1 ROW ONLY;"

        );
        $stmt->execute();
        $tplVars['pocasi_cas'] = $stmt->fetch();

        $stmt = $this->db->prepare('
        SELECT cidlo_teploty.teplota, cidlo_vlhkosti.vlhkost FROM mereni
        FULL JOIN cidlo_teploty
        USING(id_mereni)
        FULL JOIN cidlo_vlhkosti
        USING(id_mereni)
        WHERE cidlo_vlhkosti.id_mereni IS NOT NULL OR cidlo_teploty.id_mereni IS NOT NULL
        ORDER BY mereni DESC
        FETCH FIRST 1 ROW ONLY;
        ');

        $stmt->execute();


        $tplVars['posledni'] = $stmt->fetch();
        $tplVars['posledni']['vlhkost'] *= 100; //udaj je v db od 0 do 1

        $stmt = $this->db->prepare(
            "SELECT to_char(mereni, 'DD.MM. YYYY - HH24:MI:SS') AS cas FROM mereni
        FULL JOIN cidlo_svetla
        USING(id_mereni)
        WHERE stav_dne = 'vychod'
        ORDER BY(mereni) DESC
        FETCH FIRST 1 ROW ONLY;"
        );
        $stmt->execute();


        $tplVars['posledniVychod'] = $stmt->fetch();

        $stmt = $this->db->prepare(
            "SELECT to_char(mereni, 'DD.MM. YYYY - HH24:MI:SS') AS cas FROM mereni
        FULL JOIN cidlo_svetla
        USING(id_mereni)
        WHERE stav_dne = 'zapad'
        ORDER BY(mereni) DESC
        FETCH FIRST 1 ROW ONLY;"
        );
        $stmt->execute();

        $tplVars['posledniZapad'] = $stmt->fetch();


        $stmt = $this->db->prepare(
            "SELECT stav, id_cidlo FROM cidlo
            WHERE nazev = 'vlhkost';"
        );
        $stmt->execute();

        $tplVars['stavCidloVlhkost'] = $stmt->fetch();

        $stmt = $this->db->prepare(
            "SELECT stav, id_cidlo FROM cidlo
            WHERE nazev = 'teplota';"
        );
        $stmt->execute();

        $tplVars['stavCidloTeplota'] = $stmt->fetch();

        $stmt = $this->db->prepare(
            "SELECT stav, id_cidlo FROM cidlo
            WHERE nazev = 'svetlo';"
        );
        $stmt->execute();

        $tplVars['stavCidloSvetlo'] = $stmt->fetch();


        $stmt = $this->db->prepare(
            "SELECT datum, datum_zapad, datum_vychod, to_char(datum, 'DD.MM. YYYY') AS datum_char, cas_vychod, cas_zapad, to_seconds(cas_vychod) AS sec_vychod, to_seconds(cas_zapad) AS sec_zapad FROM (SELECT DISTINCT to_date(to_char(mereni, 'DD.MM. YYYY'), 'DD.MM. YYYY') AS datum FROM mereni
INNER JOIN cidlo_svetla
USING(id_mereni)
ORDER BY datum DESC
FETCH FIRST 7 ROWS ONLY) AS seznam
FULL JOIN (SELECT to_date(to_char(mereni, 'DD.MM. YYYY'), 'DD.MM. YYYY') AS datum_zapad, to_char(mereni, 'HH24:MI:SS') AS cas_zapad FROM mereni
INNER JOIN cidlo_svetla
USING(id_mereni)
WHERE stav_dne = 'zapad') AS cas_zapad_tab
ON seznam.datum = cas_zapad_tab.datum_zapad
FULL JOIN (SELECT to_date(to_char(mereni, 'DD.MM. YYYY'), 'DD.MM. YYYY') AS datum_vychod, to_char(mereni, 'HH24:MI:SS') AS cas_vychod FROM mereni
INNER JOIN cidlo_svetla
USING(id_mereni)
WHERE stav_dne = 'vychod') AS cas_vychod_tab
ON seznam.datum = cas_vychod_tab.datum_vychod
ORDER BY datum ASC;"
        );
        $stmt->execute();

        $tplVars['stavyDne'] = $stmt->fetchAll();


        $string = 24;
        $tplVars['grafText'] = "24 hodin";

        if (isset($_GET['drop_down'])) {
            switch ($_GET['drop_down']) {
                case 7:
                    $string = 7 * 24;
                    $tplVars['grafText'] = "7 dní";
                    break;
                case 30:
                    $string = 30 * 24;
                    $tplVars['grafText'] = "30 dní";
                    break;
                default:
                    $string = 24;
                    $tplVars['grafText'] = "24 hodin";
                    break;
            }
        }

        $stmt = $this->db->prepare("SELECT to_char(mereni, 'HH24:MI') AS cas, teplota, vlhkost FROM mereni
        FULL JOIN cidlo_teploty
        USING(id_mereni)
        FULL JOIN cidlo_vlhkosti
        USING(id_mereni)
        WHERE cidlo_vlhkosti.id_mereni IS NOT NULL OR cidlo_teploty.id_mereni IS NOT NULL
        ORDER BY mereni DESC
        FETCH FIRST " . $string . " ROWS ONLY
        ;");


        $stmt->execute();

        $tplVars['graf'] = $stmt->fetchAll();


    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

        exit($e->getMessage());
    }


    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');

$app->post('/offCidlo', function (Request $request, Response $response, $args) {

    $input = $request->getParsedBody();

    try {
        $stmt = $this->db->prepare("UPDATE cidlo SET stav = 'off' WHERE id_cidlo = :id_cidlo;
");
        $stmt->bindValue(':id_cidlo', $input['id_cidlo']);

        $stmt->execute();

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

    }


    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('offCidlo');

$app->post('/onCidlo', function (Request $request, Response $response, $args) {

    $input = $request->getParsedBody();

    try {
        $stmt = $this->db->prepare("UPDATE cidlo SET stav = 'on' WHERE id_cidlo = :id_cidlo;
");
        $stmt->bindValue(':id_cidlo', $input['id_cidlo']);

        $stmt->execute();

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

    }


    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('onCidlo');

$app->get('/api/cidla', function (Request $request, Response $response, $args) {
    // Render index view
    try {
        $stmt = $this->db->query("SELECT * FROM cidlo;");
        $stmt->execute();

        $cidla = $stmt->fetchAll();

        $response_data['cidla'] = $cidla;
        $response->write(json_encode($response_data));

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
    }


    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
})->setName('apiCidla');


$app->post('/api/merit_ted', function (Request $request, Response $response, $args) {

    $input = $request->getParsedBody();

    try {

        $json = '{
                    "mereni":{
                            "mereni":"2020-04-29 13:05:00",
                            "poznamka":"zmereno manualne",
                            "teplota":17,
                            "vlhkost":"0.39"
                            }
                    }';


        $mereni = json_decode($json, true);

        $stmt = $this->db->prepare("INSERT INTO mereni (mereni, poznamka) VALUES(:mereni, :poznamka);");

        $stmt->bindValue(':mereni', $mereni['mereni']['mereni']);
        $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);

        $stmt->execute();

        $stmt = $this->db->prepare("SELECT id_mereni FROM mereni
                                    ORDER BY mereni DESC
                                    FETCH FIRST 1 ROW ONLY;");

        $stmt->execute();

        $poslMereniId = $stmt->fetch();

        if(isset($mereni['mereni']['teplota'])){
            $stmt = $this->db->prepare("INSERT INTO cidlo_teploty (id_mereni, teplota, poznamka) VALUES(:id_mereni, :teplota, :poznamka);");
            $stmt->bindValue(':teplota', $mereni['mereni']['teplota']);
            $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);
            $stmt->bindValue(':id_mereni', $poslMereniId['id_mereni']);

            $stmt->execute();
        }

        if(isset($mereni['mereni']['vlhkost'])){

            $stmt = $this->db->prepare("INSERT INTO cidlo_vlhkosti (id_mereni, vlhkost, poznamka) VALUES(:id_mereni, :vlhkost, :poznamka);");

            $stmt->bindValue(':vlhkost', $mereni['mereni']['vlhkost']);
            $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);
            $stmt->bindValue(':id_mereni', $poslMereniId['id_mereni']);


            $stmt->execute();
        }


    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

    }


    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('merit_ted');

$app->post('/api/mereni', function (Request $request, Response $response, $args) {

    try {

        $json = file_get_contents('php://input');

        $mereni = json_decode($json, true);

        $stmt = $this->db->prepare("INSERT INTO mereni (mereni, poznamka) VALUES(:mereni, :poznamka);");

        $stmt->bindValue(':mereni', $mereni['mereni']['mereni']);
        $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);

        $stmt->execute();

        $stmt = $this->db->prepare("SELECT id_mereni FROM mereni
                                    ORDER BY mereni DESC
                                    FETCH FIRST 1 ROW ONLY;");

        $stmt->execute();

        $poslMereniId = $stmt->fetch();

        if(isset($mereni['mereni']['teplota'])){
        $stmt = $this->db->prepare("INSERT INTO cidlo_teploty (id_mereni, teplota, poznamka) VALUES(:id_mereni, :teplota, :poznamka);");
        $stmt->bindValue(':teplota', $mereni['mereni']['teplota']);
        $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);
        $stmt->bindValue(':id_mereni', $poslMereniId['id_mereni']);

        $stmt->execute();
    }

        if(isset($mereni['mereni']['vlhkost'])){

        $stmt = $this->db->prepare("INSERT INTO cidlo_vlhkosti (id_mereni, vlhkost, poznamka) VALUES(:id_mereni, :vlhkost, :poznamka);");

        $stmt->bindValue(':vlhkost', $mereni['mereni']['vlhkost']);
        $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);
        $stmt->bindValue(':id_mereni', $poslMereniId['id_mereni']);


        $stmt->execute();
    }

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

    }


    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('mereni');

$app->post('/api/mereni_den', function (Request $request, Response $response, $args) {

    try {

        $json = file_get_contents('php://input');

        $mereni = json_decode($json, true);

        $stmt = $this->db->prepare("INSERT INTO mereni (mereni, poznamka) VALUES(:mereni, :poznamka);");

        $stmt->bindValue(':mereni', $mereni['mereni']['mereni']);
        $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);

        $stmt->execute();

        $stmt = $this->db->prepare("SELECT id_mereni FROM mereni
                                    ORDER BY mereni DESC
                                    FETCH FIRST 1 ROW ONLY;");

        $stmt->execute();

        $poslMereniId = $stmt->fetch();

        $stmt = $this->db->prepare("INSERT INTO cidlo_svetla (id_mereni, stav_dne, poznamka) VALUES(:id_mereni, :stav_dne, :poznamka);");
        $stmt->bindValue(':stav_dne', $mereni['mereni']['stav_dne']);
        $stmt->bindValue(':poznamka', $mereni['mereni']['poznamka']);
        $stmt->bindValue(':id_mereni', $poslMereniId['id_mereni']);

        $stmt->execute();


    } catch (Exception $e) {
        $this->logger->error($e->getMessage());

    }


    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('mereni_den');

$app->get('/api/test', function (Request $request, Response $response, $args) {
    // Render index view
    try {
        $stmt = $this->db->query("SELECT * FROM mereni
        FULL JOIN cidlo_teploty
        USING(id_mereni)
        FULL JOIN cidlo_vlhkosti
        USING(id_mereni)
        WHERE cidlo_vlhkosti.id_mereni IS NOT NULL OR cidlo_teploty.id_mereni IS NOT NULL
        ORDER BY mereni DESC
        FETCH FIRST 1 ROW ONLY;");
        $stmt->execute();

        $mereni = $stmt->fetch();

        $response_data['mereni'] = $mereni;
        $response->write(json_encode($response_data));

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
    }


    return $response->withHeader('Content-type', 'application/json')->withStatus(200);
})->setName('test');
