Projekt: Meteostanice

Funkce:
-Zaznamenávání západu a východu slunce, ukládání hodnot do tabulek, výpočet posunu času západu a východu slunce
-Měření vlhkosti ovzduší a teploty každých X minut
-Možnost z UI vybrat čas měření
-Možnost z UI odpojit čidla